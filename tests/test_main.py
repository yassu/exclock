from __future__ import annotations

from dataclasses import dataclass
from typing import Optional

from pytest import raises

from exclock.main import main


@dataclass
class TestOptions:
    show_list: bool = False
    time: Optional[str] = None
    ring_filename: Optional[str] = None
    with_traceback: bool = False


def test_main():
    with raises(SystemExit):
        main()


# @patch('exclock.main.executable', return_value=True)
# @patch('exclock.main.show_list_main', return_value=None)
# def test_main_show_list_main(show_list_main_mock, executable_mock):
#     main(show_list_mock=True)
#     show_list_main_mock.assert_called_once()


# @patch('exclock.main.executable', return_value=True)
# @patch('exclock.main.specified_time_main', return_value=None)
# def test_main_specified_time_main(specified_time_main_mock, executable_mock):
#     main(time_mock='20')
#
#     specified_time_main_mock.assert_called_once()


# @patch('exclock.main.executable', return_value=True)
# @patch('exclock.main.play_clock_main', return_value=None)
# def test_main_play_clock_main(play_clock_main_mock, executable_mock, opt_parse_args_mock):
#     main(clock_filename_mock='pomodoro')
#
#     play_clock_main_mock.assert_called_once()
