from unittest.mock import patch

import pytest

from exclock.entities import ClockTimer, Sound, get_time_delta_from_str, inner_str
from exclock.util import SOUND_DIR_IN_PROG

GET_TIME_FROM_STR_PARAMS = [
    ('0', 0), ('12s', 12), ('2m12s', 2 * 60 + 12), ('25m', 25 * 60), ('30s', 30)
]


@pytest.mark.parametrize('s, expect', GET_TIME_FROM_STR_PARAMS)
def test_get_time_delta_from_str(s: str, expect: int):
    assert get_time_delta_from_str(s) == expect


def test_inner_str():
    d = {'count': 3}
    s = 'abc{count*5}def{count*7}ghi'
    assert inner_str(s, d) == 'abc15def21ghi'


def test_Sound_init():
    sound = Sound(
        message='DummyLabel',
        sound_filename='abc.mp3',
    )
    assert sound.message == 'DummyLabel'
    assert sound.sound_filename.endswith('abc.mp3')


def test_Sound_play():
    with patch('exclock.sound_player.Player.play', return_value=None) as play_mock:
        sound = Sound(message='DummyLabel', sound_filename=str(SOUND_DIR_IN_PROG / 'ring.mp3'))
        sound.play()

    assert play_mock.call_count == 1


def test_Sound_from_dict():
    arg = {
        'message': 'dummy_label',
        'sound_filename': 'dummy_sound.mp3',
    }
    expected = Sound(
        message='dummy_label',
        sound_filename='dummy_sound.mp3',
    )
    sound = Sound.from_dict(arg)

    assert expected.message == sound.message
    assert expected.sound_filename == sound.sound_filename


def test_ClockTimer_from_dict():
    arg = {
        'sounds': {},
        'loop': 1,
        'title': 'abc',
    }
    clock_timer = ClockTimer.from_dict(arg)

    assert clock_timer.sounds == {}
    assert clock_timer.loop == 1


def test_ClockTimer_from_dict2():
    arg = {
        'sounds':
            {
                '10': {
                    'message': 'dummy_label',
                    'sound_filename': 'dummy_sound.mp3',
                },
                '20': {
                    'message': 'dummy_label2',
                    'sound_filename': 'dummy_sound2.mp3',
                }
            },
        'loop': 2,
        'title': 'abc',
    }
    clock_timer = ClockTimer.from_dict(arg)

    assert clock_timer.sounds == {
        10: Sound(
            message='dummy_label',
            sound_filename='dummy_sound.mp3',
        ),
        20: Sound(
            message='dummy_label2',
            sound_filename='dummy_sound2.mp3',
        ),
    }
    assert clock_timer.loop == 2


def test_ClockTimer_from_dict3():
    ''' loopのデフォルト値が1であること '''
    arg = {
        'sounds':
            {
                '10': {
                    'message': 'dummy_label',
                    'sound_filename': 'dummy_sound.mp3',
                },
                '20': {
                    'message': 'dummy_label2',
                    'sound_filename': 'dummy_sound2.mp3',
                }
            },
        'title': 'abc',
    }
    clock_timer = ClockTimer.from_dict(arg)

    assert clock_timer.loop == 1
