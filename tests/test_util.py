from contextlib import redirect_stdout
from io import StringIO
from pathlib import Path
from typing import Tuple
from unittest.mock import patch

import pytest

from exclock.util import (
    CLOCK_DIR_IN_PROG,
    CLOCK_DIR_IN_SYS,
    SOUND_DIR_IN_PROG,
    SOUND_DIR_IN_SYS,
    convert_specific_time_to_time_delta,
    get_clock_basenames,
    get_real_json_filename,
    get_real_sound_filename,
    get_specific_time_from_str,
    get_time_delta_from_str,
    is_specific_time_str,
    is_time_delta_str,
    notify,
    notify_all,
)


def test_ASSET_FILES_IN_PROG():
    ''' IN_PROGのファイル/ディレクトリが存在することのテスト '''
    assert CLOCK_DIR_IN_PROG.exists()
    assert SOUND_DIR_IN_PROG.exists()


GET_CLOCK_BASENAMES_PARAMS = [
    ('pomodoro', True),
    ('3m', True),
    ('not_exists', False),
]


@pytest.mark.parametrize('basename, expect', GET_CLOCK_BASENAMES_PARAMS)
def test_get_clock_basenames(basename: str, expect: bool):
    assert (basename in get_clock_basenames()) is expect


def test_get_clock_basename2():
    assert get_clock_basenames() == ['3m', '5m', 'clock_in_sys.json', 'pomodoro', 'warning']


def test_get_clock_basenames2():
    ''' 同じ名前のclockは一度だけ登場すること '''
    basenames = get_clock_basenames()
    assert 'pomodoro' in basenames
    assert basenames.count('pomodoro') == 1


def test_get_clock_basenames3():
    ''' ソート済みであること '''
    basenames = get_clock_basenames()
    assert basenames == sorted(basenames)


def test_get_clock_basenames4():
    ''' json5以外の拡張子は省略されないこと '''
    assert 'clock_in_sys.json' in get_clock_basenames()


IS_TIME_STR_PARAMS = [
    ('', False), ('1', True), ('abc', False), ('17m', True), ('12s', True), ('17m12s', True),
    ('17m12sklm', False)
]


@pytest.mark.parametrize('s, expect', IS_TIME_STR_PARAMS)
def test_is_time_delta_str(s: str, expect: bool):
    assert is_time_delta_str(s) is expect


GET_TIME_DELTA_FROM_STR_PARAMS = [
    ('1', 1),
    ('17m', 17 * 60),
    ('12s', 12),
    ('17m12s', 17 * 60 + 12),
]


@pytest.mark.parametrize('s, expect', GET_TIME_DELTA_FROM_STR_PARAMS)
def test_get_time_delta_from_str(s: str, expect: int):
    assert get_time_delta_from_str('3') == 3


IS_SPECIFIC_TIME_STR_PARAMS = [
    ('1 : 59', True),
    ('1', False),
    ('1 : 2 : 3 : 4', False),
    ('2 : 1a', False),
    ('1 : 60', False),
    ('13 : 59', True),
    ('59 : 59 : 59', True),
    ('60 : 59 : 59', True),
    ('60 : 59a : 59', False),
    ('59 : 59 : 60', False),
    ('59 : 60 : 59', False),
]


@pytest.mark.parametrize('s, expect', IS_SPECIFIC_TIME_STR_PARAMS)
def test_is_specific_time_str(s: str, expect: bool):
    assert is_specific_time_str(s) is expect


GET_SPECIFIC_TIME_STR_PARAMS = [
    ('1 : 59', (1, 59, 0)),
    ('13 : 59', (13, 59, 0)),
    ('59 : 59 : 59', (59, 59, 59)),
    ('60 : 59 : 59', (60, 59, 59)),
]


@pytest.mark.parametrize('s, expect', GET_SPECIFIC_TIME_STR_PARAMS)
def test_get_specific_time_from_str(s: str, expect: int):
    assert get_specific_time_from_str(s) == expect


CONVERT_SPECIFIC_TIME_TO_TIME_DELTA_PARAMS = [
    ((3, 0, 0), (1, 0, 0), 2 * 60 * 60), ((1, 0, 0), (3, 0, 0), (24 - 2) * 60 * 60)
]


@pytest.mark.parametrize('time_, now_, expect', CONVERT_SPECIFIC_TIME_TO_TIME_DELTA_PARAMS)
def test_convert_specific_time_to_time_delta(
        time_: Tuple[int, int, int], now_: Tuple[int, int, int], expect: int):
    assert convert_specific_time_to_time_delta(time_, now_) == expect


def test_get_real_json_filename():
    absolute_path = str(CLOCK_DIR_IN_PROG / 'pomodoro.json5')
    assert get_real_json_filename(absolute_path) == str(absolute_path)


def test_get_real_json_filename2():
    filename = get_real_json_filename('pomodoro')
    assert Path(filename).exists()


def test_get_real_json_filename3():
    assert get_real_json_filename('warning.json5') == str(CLOCK_DIR_IN_SYS / 'warning.json5')


def test_get_real_sound_filename():
    ''' プログラムの中のring.mp3のフルパスを引数として渡すとring.mp3のフルパスが返ってくること '''
    assert get_real_sound_filename(str(SOUND_DIR_IN_PROG / 'ring.mp3')) == str(
        SOUND_DIR_IN_PROG / 'ring.mp3')


def test_get_real_sound_filename2():
    assert get_real_sound_filename('ring.mp3') == \
        str(SOUND_DIR_IN_SYS / 'ring.mp3')


@patch('os.system')
def test_notify(os_system):
    notify(title='DummyTitle', message='DummyMessage')

    cmd = list(os_system.mock_calls[0][1])[0]
    assert ('xmessage' in cmd) or ('terminal-notifier' in cmd)
    assert 'DummyTitle' in cmd
    assert 'DummyMessage' in cmd
    assert os_system.call_count == 1


@patch('os.system')
def test_notify_all(os_system):
    out_io = StringIO()
    with redirect_stdout(out_io):
        notify_all(
            title='DummyTitle',
            message='DummyMessage',
            spend_sec=0,
            is_first=False,
            show_message=True,
        )
    out_output = out_io.getvalue()

    cmd = list(os_system.mock_calls[0][1])[0]
    assert ('xmessage' in cmd) or ('terminal-notifier' in cmd)
    assert cmd.count('DummyTitle') == 1
    assert cmd.count('DummyMessage') == 1

    assert out_output.count('DummyTitle') == 0
    assert out_output.count('DummyMessage') == 1


@patch('exclock.util.notify')
def test_notify_all_in_no_show_message_case(exclock_notify):
    out_io = StringIO()
    with redirect_stdout(out_io):
        notify_all(
            title='DummyTitle',
            message='DummyMessage',
            spend_sec=0,
            is_first=False,
            show_message=False,
        )
    assert exclock_notify.called is False


@patch('os.system')
def test_notify_all2(os_system):
    ''' 初回はxmessageが呼ばれないこと '''
    out_io = StringIO()
    with redirect_stdout(out_io):
        notify_all(
            title='DummyTitle',
            message='DummyMessage',
            spend_sec=0,
            is_first=True,
            show_message=True,
        )

    assert os_system.call_count == 0


@patch('os.system')
@patch('exclock.util.time_sleep')
def test_notify_all3(time_sleep, os_system):
    ''' notify_allで時間を測っていること '''
    out_io = StringIO()
    with redirect_stdout(out_io):
        notify_all(
            title='DummyTitle',
            message='DummyMessage',
            spend_sec=20,
            is_first=True,
            show_message=True,
        )

    assert sum([call[1][0] for call in time_sleep.mock_calls]) == 20
