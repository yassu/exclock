from contextlib import redirect_stdout
from io import StringIO
from unittest.mock import patch

import freezegun
import pytest

from exclock.mains.specified_time_main import get_ring_filename, get_spend_sec, main
from exclock.util import SOUND_DIR_IN_PROG, get_real_sound_filename

GET_TIME_PARAMS = [
    ('12', 12),
    ('12s', 12),
    ('1m2s', 1 * 60 + 2),
    ('2m', 60 * 2),
    ('abc', None),
    ('00:01', 60),
    ('23:59', 23 * 60 * 60 + 59 * 60),
]


@freezegun.freeze_time('2020-01-01')
@pytest.mark.parametrize('s, expect', GET_TIME_PARAMS)
def test_get_spend_sec(s, expect):
    assert get_spend_sec(s) == expect


GET_RING_FILENAME_PARAMS = [
    (None, 'ring.mp3'),
    ('ring.mp3', 'ring.mp3'),
]


@pytest.mark.parametrize('ring_filename, sound_filename', GET_RING_FILENAME_PARAMS)
def test_get_ring_filename(ring_filename, sound_filename):
    assert get_ring_filename(ring_filename) == get_real_sound_filename(sound_filename)


def test_main_illegal_case():
    with pytest.raises(SystemExit):
        main('abc', None)


@patch('exclock.mains.specified_time_main.notify_all')
@patch('exclock.mains.specified_time_main.Player.play')
def test_main(play_mock, notify_all_mock):
    stdio = StringIO()
    with redirect_stdout(stdio):
        main(time_='10', ring_filename=str(SOUND_DIR_IN_PROG / 'ring.mp3'))
    assert stdio.getvalue().strip() == 'bye'

    play_mock.assert_called_once()
    notify_all_mock.assert_called_once()


@patch('exclock.mains.specified_time_main.notify_all')
@patch('exclock.mains.specified_time_main.Player.play')
def test_main_specific_time_case(play_mock, notify_all_mock):
    main(time_='00:00', ring_filename=str(SOUND_DIR_IN_PROG / 'ring.mp3'))

    play_mock.assert_called_once()
    notify_all_mock.assert_called_once()


def test_main_not_found_ring_filename():
    with pytest.raises(SystemExit):
        main(time_='10', ring_filename=str(SOUND_DIR_IN_PROG / 'abc.mp3'))
