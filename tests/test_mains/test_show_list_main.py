from contextlib import redirect_stdout
from io import StringIO

from exclock.mains.show_list_main import main


def test_show_list_main():
    stdout = StringIO()

    with redirect_stdout(stdout):
        main()

    outputs = set(stdout.getvalue().split('\n'))
    assert 'pomodoro' in outputs
    assert 'clock_in_sys.json' in outputs
