from unittest.mock import patch

import pytest

from exclock.mains.play_clock_main import check_raw_clock, get_title_from_json_filename, main
from exclock.util import CLOCK_DIR_IN_PROG

GET_TITLE_FROM_JSON_FILENAME_PARAMS = [
    ('abc.json5', 'Abc'), ('abc', 'Abc'), ('~/repo/abc.json', 'Abc')
]


@pytest.mark.parametrize('json_filename, expect', GET_TITLE_FROM_JSON_FILENAME_PARAMS)
def test_get_title_from_json_filename(json_filename: str, expect: str):
    assert get_title_from_json_filename(json_filename) == expect


CHECK_RAW_CLOCK_EXCEPTION_PARAMS = [
    (
        {
            'sounds': {
                '0': {
                    'message': 'dummy',
                    'sound_filename': '/tmp/dummy_tmp',
                }
            },
        },
        FileNotFoundError,
        'is not found',
    ),
    ('abcdef', ValueError, "doesn't mean dict"),
    ({
        'loop': 1
    }, ValueError, 'sounds.*required field'),
    ({
        'sounds': 0,
    }, ValueError, 'sounds.*must be of dict type'),
    (
        {
            'sounds': {
                'abc': {
                    'message': 'dummy',
                    'sound_filename': 'silent.mp3'
                }
            },
        }, ValueError, 'abc.*Must be an time string'),
    ({
        'sounds': {
            '0': []
        },
    }, ValueError, 'must be of dict type'),
    ({
        'sounds': {
            '0': {
                'sound_filename': 'silent.mp3'
            },
        },
    }, ValueError, 'message.*required field'),
    (
        {
            'sounds': {
                '0': {
                    'message': 3,
                    'sound_filename': 'silent.mp3'
                },
            },
        }, ValueError, 'message.*must be of string type'),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 3,
                    'message': 'test message',
                },
            },
        }, ValueError, 'sound_filename.*must be of string type'),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'loop': '1',
        }, ValueError, 'loop.*must be of integer type'),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'loop': 2,
        }, None, ''),
    (
        {
            'sounds': {
                '3m20s': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'loop': 2,
        }, None, ''),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'loop': None,
        }, None, ''),
    ({
        'sounds': {
            '0': {
                'message': 'test message',
            },
        },
        'loop': None,
    }, None, ''),
    (
        {
            'title': 1,
            'sounds': {
                '0': {
                    'message': 'message1',
                },
                '12': {
                    'message': 'message2',
                },
            }
        }, ValueError, 'title.*must be of string type'),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'show_message': True,
            'loop': 2,
        }, None, ''),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'show_message': False,
            'loop': 2,
        }, None, ''),
    (
        {
            'sounds': {
                '0': {
                    'sound_filename': 'ring.mp3',
                    'message': 'test message',
                },
            },
            'show_message': 1,
            'loop': 2,
        }, ValueError, 'show_message.*must be of boolean type'),
]


@pytest.mark.parametrize('d, err_cls, match', CHECK_RAW_CLOCK_EXCEPTION_PARAMS)
def test_check_raw_clock_exception(d, err_cls: type, match: str):
    if err_cls:
        with pytest.raises(err_cls, match=match):
            check_raw_clock(d)
    else:
        check_raw_clock(d)


@patch('exclock.main.executable', return_value=True)
@patch('exclock.sound_player.Player.play', return_value=None)
def test_main(play_mock, executable_mock):
    clock_filename = str(CLOCK_DIR_IN_PROG / 'warning.json5')
    main(clock_filename)

    play_mock.assert_called_once()


def test_main_json_filename_not_found():
    clock_filename = str(CLOCK_DIR_IN_PROG / 'abc.json5')
    with pytest.raises(SystemExit):
        main(clock_filename)
