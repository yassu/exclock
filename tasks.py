import term
import toml
from invoke import task


def print_title(title: str) -> None:
    term.writeLine('running ' + title, term.green)


def _run_commands(ctx, commands):
    for command in commands:
        print(command)
        ctx.run(command)


def load_pyproject():
    with open('pyproject.toml') as f:
        return toml.load(f)


@task(aliases=['f'])
def format(ctx):
    print_title('yapf')
    ctx.run('yapf -i **/*.py')
    print_title('isort')
    ctx.run('isort **/*.py')


@task(aliases=['t'])
def test(ctx):
    ''' テストする '''
    print_title('pytest')
    ctx.run('pytest -vv --cov=exclock tests')
    print_title('pytest with cov-report')
    ctx.run('pytest --cov=exclock tests --cov-report=html > /dev/null 2>&1')
    print_title('mypy')
    ctx.run('mypy tasks.py exclock tests')
    print_title('flake8')
    ctx.run('flake8 .')
    print_title('isort')
    ctx.run('isort --check-only exclock/ tests/')


@task(test)
def release(ctx):
    ''' release this project '''
    project_info = load_pyproject()
    ver = 'v' + project_info['tool']['poetry']['version']

    _run_commands(ctx, [
        'poetry install',
        'poetry publish --build',
        f'git tag {ver}',
        'git push origin HEAD --tags',
    ])
